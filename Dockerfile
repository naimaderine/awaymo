FROM python:3

WORKDIR /usr/src/app

COPY . .
RUN pip install --no-cache-dir -r requirements.txt && \
        python manage.py migrate awaymo --settings=awaymo.settings

CMD ["python", "./manage.py", "runserver", "0.0.0.0:8000", "--settings=awaymo.settings"]
