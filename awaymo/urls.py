from django.urls import path
from .views import routing, Content
from django.conf.urls import include

urlpatterns = [
    path('steps/', include(routing)),
    path('content/', Content.as_view()),
]
