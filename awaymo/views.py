from rest_framework import views
from .serializers import Step1Serializer, Step2Serializer, CustomUserSerializer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, BasePermission, AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.reverse import reverse
from django.contrib.auth import get_user_model
from django.conf.urls import url
from .models import CustomUser
import time
import requests
import logging


logger = logging.getLogger(__name__)

class IsActivated(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.active

class Content(views.APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,IsActivated)
    def get(self, request):
        return Response({"content": "top secret"})

class Step1(views.APIView):
    def get(self, request):
        return Response({'message': 'enter username and password'})

    def post(self, request):
        data = Step1Serializer(request.data).data
        user = get_user_model().objects.create(**data)
        token_key = Token.objects.create(user=user).key
        return Response({
            'token': token_key,
            'next': reverse('step2', request=request)
        })

class Step2(views.APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = CustomUserSerializer(request.user).data
        return Response({
            "user": user,
            "message": "enter address"
        })

    def post(self, request):
        data = Step2Serializer(request.data).data
        res = CustomUser.objects.filter(id=request.user.id).update(**data)
        return Response({'next': reverse('step3', request=request), "result": res})

class Step3(views.APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        return Response({"message": "In order to validate card send empty POST"})

    def post(self, request):
        for _ in range(5):
            headers = {'content-type': 'application/json'}
            resp = requests.post('https://functionapp20180527095701.azurewebsites.net/api/CreditCheckCall', json={
                "first_name": request.user.first_name,
                "last_name": request.user.last_name
            }, headers=headers)
            if resp.ok and resp.json().get('accepted'):
                CustomUser.objects.filter(id=request.user.id).update(active=True)
                Response({'result': "success"})
            time.sleep(1)
        return Response({'result': "failed"})

routing = [
    url('1', Step1.as_view(), name='step1'),
    url('2', Step2.as_view(), name='step2'),
    url('3', Step3.as_view(), name='step3'),
]